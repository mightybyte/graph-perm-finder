{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
module Frontend where

------------------------------------------------------------------------------
import           Control.Monad
import           Control.Monad.Fix
import           Control.Monad.Trans
import qualified Data.Array.IArray as IA
import           Data.Array.MArray
import           Data.Array.IO
import           Data.Array.ST
import           Data.Array.Unboxed
import           Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.PQueue.Prio.Max (MaxPQueue)
import qualified Data.PQueue.Prio.Max as MaxPQ
import           Data.PQueue.Prio.Min (MinPQueue)
import qualified Data.PQueue.Prio.Min as MinPQ
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Vector as V
import           Immutable.Shuffle
import           Obelisk.Frontend
import           Obelisk.Generated.Static
import           Obelisk.Route
import           Obelisk.Route.Frontend
import           Reflex.Dom.Core
import           Reflex.Network
import           System.Random
import           Text.Read
------------------------------------------------------------------------------
import           Common.Route
------------------------------------------------------------------------------

tshow :: Show a => a -> Text
tshow = T.pack . show

frontend :: Frontend (R FrontendRoute)
frontend = Frontend
  { _frontend_head = appHead
  , _frontend_body = appBody
  }

appBody
  :: (ObeliskWidget js t (R FrontendRoute) m)
  => m ()
appBody = do
  divClass "ui fixed inverted menu" nav
  elAttr "div" ("class" =: "ui main container" <> "style" =: "width: 1124px;") $ do
    permGame
  footer

footer
  :: (DomBuilder t m)
  => m ()
footer = do
    divClass "ui inverted vertical footer segment" $ do
      divClass "ui center aligned container" $ do
        elAttr "img" ("src" =: static @"kadena-k-logo.png" <>
                    "class" =: "ui centered mini image" <>
                    "alt" =: "Kadena" ) blank
        divClass "ui horizontal inverted small divided link list" $ do
          lnk "Discord" "https://discordapp.com/invite/bsUcWmX"
          lnk "Twitter" "https://twitter.com/kadena_io"
          lnk "Medium" "https://medium.com/kadena-io/"
          lnk "GitHub" "https://github.com/kadena-io/"
          lnk "YouTube" "https://www.youtube.com/KadenaBlockchain"
  where
    lnk nm url = elAttr "a" ("class" =: "item" <> "href" =: url) $ text nm


type Graph = Map Int [Int]
type Perm = Map Int Int
type Ink = Int

permToFunc :: Perm -> (Int -> Int)
permToFunc p = (p M.!)

permuteGraph :: Graph -> Perm -> Graph
permuteGraph g p = M.fromList $ map f $ M.toList g
  where
    f (f,ts) = (p M.! f, map (p M.!) ts)

calcInk :: Graph -> (Int -> Int) -> Ink
calcInk g permFunc = sum $ map distFrom $ M.toList g
  where
    distFrom (a,bs) = sum $ map (abs . (permFunc a -) . permFunc) bs

--theGraph = petersenGraph
theGraph = twelveChainC

mkIdPerm :: Graph -> Perm
mkIdPerm m = M.fromList $ zip cs cs
  where
    cs = M.keys m

permGame :: (DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m, Prerender js t m) => m ()
permGame = void $ prerender blank $ do
    elAttr "div" ("class" =: "ui container" <> "style" =: "width: 1100px") $ mdo
      el "h1" $ do
        text "Find the best looking chain graph!"
      (e,_) <- el "p" $ elAttr' "button" ("class" =: "ui button") $ text "New Random Chain Graph"
      ti <- textInput def
      (setBtn,_) <- el "p" $ elAttr' "button" ("class" =: "ui button") $ text "Set Permutation"
      (levt, revt) <- el "div" $ do
        (leftBtn,_) <- el "p" $ elAttr' "button" ("class" =: "ui button") $ text "<-"
        (rightBtn,_) <- el "p" $ elAttr' "button" ("class" =: "ui button") $ text "->"
        display cursorLoc
        return (domEvent Click leftBtn, domEvent Click rightBtn)
      g0 <- liftIO newStdGen
      let handleKey kp = id
      selInd <- holdDyn Nothing $ leftmost
        [ Just <$> switch (current deClicks)
        , Nothing <$ domEvent Click e
        ]
      cursorLoc <- foldDyn ($) Nothing $ leftmost
        [ const . Just <$> fmapMaybe id (updated selInd)
        , fmap pred <$ levt
        , fmap succ <$ revt
        , const Nothing <$ domEvent Click e
        ]
      res <- foldDyn ($) (mkIdPerm theGraph, g0) $ leftmost
        [ genPerm theGraph <$ domEvent Click e
        , setPerm <$> fmapMaybe parsePerm (tag (current $ value ti) (domEvent Click setBtn))
        , (\i -> swapInds i (i-1)) <$> fmapMaybe id (tag (current cursorLoc) levt)
        , (\i -> swapInds i (i+1)) <$> fmapMaybe id (tag (current cursorLoc) revt)
        ]
      --dynState <- prerender initState (liftIO randPerm <$> domEvent Click e)
      let perm = fst <$> res

      deClicks <- divClass "ind-wrapper" $ do
        let indBox n = do
              let static = "class" =: "ind-box"
              let mkAttrs sel = if sel == Just n
                                  then "class" =: "ind-box selected"
                                  else "class" =: "ind-box"
              (e,_) <- elDynAttr' "span" (mkAttrs <$> selInd) $ text $ tshow n
              return $ (n <$ domEvent Click e)
            boxRow m = mdo
              res <- mapM (indBox . snd) $ M.toList m
              return $ leftmost res

        networkHold (boxRow (mkIdPerm theGraph)) $ boxRow <$> updated perm


      el "div" $
          void $ networkView (graphDiagram <$> perm)
      el "div" $ dynText $ (tshow . map snd . M.toList) <$> perm
      el "br" blank
      el "div" $ text "If you find a chain graph that you think looks nice, copy the above line and send it to us."

swapInds :: Int -> Int -> (Perm, StdGen) -> (Perm, StdGen)
swapInds i j pair@(p,g) = fromMaybe pair $ do
  v1 <- M.lookup i p
  v2 <- M.lookup j p
  pure (M.insert i v2 $ M.insert j v1 p, g)

setPerm :: Perm -> (Perm, StdGen) -> (Perm, StdGen)
setPerm p (_,g) = (p,g)

parsePerm :: Text -> Maybe Perm
parsePerm t = do
  inds <- readMaybe (T.unpack t)
  pure $ M.fromList $ zip [0..] inds

genPerm :: Graph -> (Perm, StdGen) -> (Perm, StdGen)
genPerm graph (_,g1) = (M.fromList $ zip allChains $ V.toList v, g2)
  where
    allChains = M.keys graph
    (v,g2) = derangement (V.fromList allChains) g1

graphDiagram
  :: (DomBuilder t m, PostBuild t m, MonadHold t m)
  => Map Int Int
  -> m ()
graphDiagram perm = do
    svgElAttr "svg" ("viewBox" =: ("0 0 1100 200") <>
                  "xmlns" =: "http://www.w3.org/2000/svg") $ do
      es <- forM allChains (\i -> rect perm i (i * blockWidth + blockWidth `div` 2) 0)
      hb <- holdDyn Nothing $ leftmost es
      forM_ allChains (linksFromBlock perm hb)
      forM_ allChains (\i -> rect perm i (i * blockWidth + blockWidth `div` 2) (blockSeparation + squareSize))

    when (perm == mkIdPerm theGraph) $
      text "This is original chain graph.  Don't send this one in.  You're trying to do better than this."
  where
    allChains = M.keys theGraph



appHead :: DomBuilder t m => m ()
appHead = do
    analytics
    el "title" $ text "PermFinder"
    css (static @"semantic.min.css")
    css (static @"css/custom.css")
    jsScript (static @"jquery-3.1.1.min.js")
    jsScript (static @"semantic.min.js")

analytics :: DomBuilder t m => m ()
analytics = do
    let gaTrackingId = "UA-127512784-7"
    let gtagSrc = "https://www.googletagmanager.com/gtag/js?id=" <> gaTrackingId
    elAttr "script" ("async" =: "" <> "src" =: gtagSrc) blank
    el "script" $ text $ T.unlines
      [ "window.dataLayer = window.dataLayer || [];"
      , "function gtag(){dataLayer.push(arguments);}"
      , "gtag('js', new Date());"
      , "gtag('config', '" <> gaTrackingId <> "');"
      ]


css :: DomBuilder t m => Text -> m ()
css url = elAttr "link" ("rel" =: "stylesheet" <> "type" =: "text/css" <> "href" =: url) blank

jsScript :: DomBuilder t m => Text -> m ()
jsScript url = elAttr "script" ("src" =: url <> "type" =: "text/javascript") blank

script :: DomBuilder t m =>  Text -> m ()
script code = elAttr "script" ("type" =: "text/javascript") $ text code

blockWidth :: Int
blockWidth = 55

blockSeparation :: Int
blockSeparation = 50

squareSize :: Int
squareSize = 40

fromPos :: Int -> Int
fromPos f = blockWidth `div` 2 + f * blockWidth

toPos :: Int -> Int
toPos t = blockWidth `div` 2 + t * blockWidth

linksFromBlock
  :: (DomBuilder t m, PostBuild t m)
  => Map Int Int
  -> Dynamic t (Maybe Int)
  -> Int
  -> m ()
linksFromBlock perm hoveredBlock from = do
  let pf = id --(perm M.!)
  let permGraph = permuteGraph theGraph perm
  let toBlocks = permGraph M.! from
      mkAttrs hb = if hb == Just (pf from)
                     then "stroke" =: "rgb(0,0,0)" <>
                          "stroke-width" =: "1.0"
                     else "stroke" =: "rgb(180,180,180)"
  svgElDynAttr "g" (mkAttrs <$> hoveredBlock) $ do
    linkFromTo (pf from) (pf from)
    mapM_ (linkFromTo (pf from)) toBlocks

rect
  :: (DomBuilder t m, PostBuild t m)
  => Map Int Int
  -> Int
  -> Int
  -> Int
  -> m (Event t (Maybe Int))
rect perm i x y = do
  (e,_) <- svgElAttr' "g" mempty $ do
    let left = x - squareSize `div` 2
    svgElAttr "rect" ("x" =: tshow left <> "y" =: tshow y <>
                      "width" =: tshow squareSize <> "height" =: tshow squareSize <>
                      "fill" =: "lightgrey") blank
    svgElAttr "text" ( --"text-anchor" =: "middle" <>
                      "x" =: tshow (left + squareSize `div` 2 - 5) <>
                      "y" =: tshow (y + squareSize `div` 2 + 5) <>
                      "stroke" =: "black") $
      text $ tshow (perm M.! i)
  return $ leftmost [Just i <$ domEvent Mouseenter e, Nothing <$ domEvent Mouseleave e]

linkFromTo :: (DomBuilder t m, PostBuild t m) => Int -> Int -> m ()
linkFromTo from to =
    svgElAttr "line" ("x1" =: (tshow $ fromPos from) <>
                   "y1" =: "40" <>
                   "x2" =: (tshow $ toPos to) <>
                   "y2" =: (tshow $ 40 + blockSeparation) ) blank

petersenGraph :: Graph
petersenGraph = M.fromList
    [ (0, [2,3,5]) -- 10
    , (1, [3,4,6]) -- 10
    , (2, [4,0,7]) -- 9
    , (3, [0,1,8]) -- 10
    , (4, [1,2,9]) -- 10
    , (5, [0,6,9]) -- 10
    , (6, [1,5,7]) -- 7
    , (7, [2,6,8]) -- 7
    , (8, [3,7,9]) -- 7
    , (9, [4,8,5]) -- 10
    ]

twelveChain :: Graph
twelveChain = M.fromList
  [(0,[1,2,3])
  ,(1,[0,2,3])
  ,(2,[0,1,4])
  ,(3,[0,1,5])
  ,(4,[2,5,6])
  ,(5,[3,4,7])
  ,(6,[4,7,8])
  ,(7,[5,6,9])
  ,(8,[6,10,11])
  ,(9,[7,10,11])
  ,(10,[8,9,11])
  ,(11,[8,9,10])
  ]

p1 :: Perm
p1 = M.fromList $ zip [0..] [0,1,2,4,3,5,7,6,8,9,10,11]
twelveChainB :: Graph
twelveChainB = M.fromList
  [(0,[1,2,3])
  ,(1,[0,4,5])
  ,(2,[0,6,7])
  ,(3,[0,8,9])
  ,(4,[1,6,8])
  ,(5,[1,7,10])
  ,(6,[2,4,11])
  ,(7,[2,5,9])
  ,(8,[3,4,10])
  ,(9,[3,7,11])
  ,(10,[5,8,11])
  ,(11,[6,9,10])
  ]

--p1 = M.fromList $ zip [0..] [0,1,2,4,3,5,7,6,8,9,10,11]
twelveChainC :: Graph
twelveChainC = M.fromList
  [(0,[1,2,4])
  ,(1,[0,3,5])
  ,(2,[0,7,6])
  ,(3,[1,7,8])
  ,(4,[0,8,9])
  ,(5,[1,6,10])
  ,(6,[2,5,9])
  ,(7,[2,3,11])
  ,(8,[4,3,10])
  ,(9,[4,6,11])
  ,(10,[5,8,11])
  ,(11,[7,9,10])
  ]

new20 :: Graph
new20 = M.fromList
  [(0,[1,5,14])
  ,(1,[0,2,15])
  ,(2,[1,3,12])
  ,(3,[2,4,8])
  ,(4,[3,5,17])
  ,(5,[0,4,6])
  ,(6,[5,7,11])
  ,(7,[6,8,18])
  ,(8,[3,7,9])
  ,(9,[8,10,14])
  ,(10,[9,11,19])
  ,(11,[6,10,12])
  ,(12,[2,11,13])
  ,(13,[12,14,16])
  ,(14,[0,9,13])
  ,(15,[1,18,19])
  ,(16,[13,17,18])
  ,(17,[4,16,19])
  ,(18,[7,15,16])
  ,(19,[10,15,17])
  ]

-- Perm applied to new20
-- [17,6,9,4,5,18,14,12,11,16,15,10,8,3,19,1,0,7,2,13]
better20 :: Graph
better20 = M.fromList
  [(0,[3,7,2])
  ,(1,[6,2,13])
  ,(2,[12,1,0])
  ,(3,[15,19,0])
  ,(4,[9,5,11])
  ,(5,[4,18,7])
  ,(6,[17,9,1])
  ,(7,[5,0,13])
  ,(8,[14,10,15])
  ,(9,[6,4,15])
  ,(10,[16,8,13])
  ,(11,[4,12,16])
  ,(12,[14,11,2])
  ,(13,[10,1,7])
  ,(14,[18,12,8])
  ,(15,[9,8,3])
  ,(16,[11,10,19])
  ,(17,[6,18,19])
  ,(18,[17,5,14])
  ,(19,[17,16,3])
  ]

-- Perm applied to new20
-- [17,5,9,4,6,18,14,12,11,16,10,8,15,3,19,1,0,7,2,13]
best20 :: Graph
best20 = M.fromList
  [(0,[3,7,2])
  ,(1,[5,2,13])
  ,(2,[12,1,0])
  ,(3,[15,19,0])
  ,(4,[9,6,11])
  ,(5,[17,9,1])
  ,(6,[4,18,7])
  ,(7,[6,0,13])
  ,(8,[14,10,15])
  ,(9,[5,4,15])
  ,(10,[16,8,13])
  ,(11,[4,12,16])
  ,(12,[14,11,2])
  ,(13,[10,1,7])
  ,(14,[18,12,8])
  ,(15,[9,8,3])
  ,(16,[11,10,19])
  ,(17,[5,18,19])
  ,(18,[17,6,14])
  ,(19,[17,16,3])
  ]

svgElDynAttr
  :: (DomBuilder t m, PostBuild t m)
  => Text
  -> Dynamic t (Map Text Text)
  -> m a
  -> m a
svgElDynAttr elTag attrs child = elDynAttrNS (Just "http://www.w3.org/2000/svg") elTag attrs child

svgElAttr
  :: (DomBuilder t m, PostBuild t m)
  => Text
  -> Map Text Text
  -> m a
  -> m a
svgElAttr elTag attrs child = svgElDynAttr elTag (constDyn attrs) child

svgElDynAttr'
  :: (DomBuilder t m, PostBuild t m)
  => Text
  -> Dynamic t (Map Text Text)
  -> m a
  -> m (Element EventResult (DomBuilderSpace m) t, a)
svgElDynAttr' elTag attrs child = elDynAttrNS' (Just "http://www.w3.org/2000/svg") elTag attrs child

svgElAttr'
  :: (DomBuilder t m, PostBuild t m)
  => Text
  -> Map Text Text
  -> m a
  -> m (Element EventResult (DomBuilderSpace m) t, a)
svgElAttr' elTag attrs child = svgElDynAttr' elTag (constDyn attrs) child

------------------------------------------------------------------------------

nav
  :: (DomBuilder t m, MonadHold t m, PostBuild t m, MonadFix m)
  => m ()
nav = do
  divClass "ui container" $ do
    elAttr "a" ("class" =: "header item" <>
                "href" =: "/" <>
                "style" =: "color: #e8098f;") $
      elAttr "img" ("class" =: "logo" <>
                    "src" =: static @"kadena-k-logo.png") $
        text "Kadena Chain Graph Contest"
    elAttr "a" ("class" =: "header item" <> "href" =: "/") $ text "Kadena Chain Graph Contest"
--    divClass "right menu" $ do
--      linkItem "About" "/about"
--      getStarted
--      learnMore

getStarted
  :: (DomBuilder t m, PostBuild t m, MonadFix m, MonadHold t m)
  => m ()
getStarted = mdo
  (e,_) <- elAttr' "div" ("class" =: "ui dropdown item") $ do
    text "Get Started"
    let mkAttrs as vis = "class" =: (if vis then (as <> " visible") else as)
    elDynAttr "div" (mkAttrs "menu transition" <$> dropdownVisible) $ do
      linkItemNewTab "Start Mining" "https://github.com/kadena-io/chainweb-miner/blob/master/README.org"
      linkItemNewTab "Download Wallet" "https://www.kadena.io/chainweaver"
      linkItemNewTab "Play Testnet Games" "http://testnet.chainweb.com/games/"
      linkItemNewTab "See Chains in 3D (experimental)" (static @"chains-3d.html")
  dropdownVisible <- holdDyn False $ leftmost
    [ True <$ domEvent Mouseenter e
    , False <$ domEvent Mouseleave e
    ]
  return ()

learnMore
  :: (DomBuilder t m, PostBuild t m, MonadFix m, MonadHold t m)
  => m ()
learnMore = mdo
  (e,_) <- elAttr' "div" ("class" =: "ui dropdown item") $ do
    text "Learn More"
    let mkAttrs as vis = "class" =: (if vis then (as <> " visible") else as)
    elDynAttr "div" (mkAttrs "menu transition" <$> dropdownVisible) $ do
      linkItemNewTab "Kadena Docs" "https://kadena-io.github.io/kadena-docs/"
      linkItemNewTab "Pact Smart Contract Tutorials" "https://pactlang.org"
      linkItemNewTab "Kadena Whitepapers" "https://kadena.io/en/whitepapers/"
  dropdownVisible <- holdDyn False $ leftmost
    [ True <$ domEvent Mouseenter e
    , False <$ domEvent Mouseleave e
    ]
  return ()

linkItem
  :: DomBuilder t m
  => Text
  -> Text
  -> m ()
linkItem nm url = do
    elAttr "a" ("href" =: url <> "class" =: "item") $ text nm

linkItemNewTab
  :: DomBuilder t m
  => Text
  -> Text
  -> m ()
linkItemNewTab nm url = do
    elAttr "a" ("href" =: url <> "target" =: "_blank" <> "class" =: "item") $ text nm

data PermStats = PermStats
  { _psBest :: MaxPQueue Ink Perm
  , _psWorst :: MinPQueue Ink Perm
  }

emptyStats :: PermStats
emptyStats = PermStats MaxPQ.empty MinPQ.empty

addToStats :: Graph -> PermStats -> (Perm, a) -> PermStats
addToStats g (PermStats b w) (p,_) = PermStats b3 w3
  where
    ink = calcInk g (p M.!)
    numExtremes = 10
    b2 = MaxPQ.insert ink p b
    b2s = MaxPQ.size b2
    b3 = if b2s > numExtremes then MaxPQ.drop (b2s - numExtremes) b2 else b2
    w2 = MinPQ.insert ink p w
    w2s = MinPQ.size w2
    w3 = if w2s > numExtremes then MinPQ.drop (w2s - numExtremes) w2 else w2

showPerm :: Perm -> [Int]
showPerm = map snd . M.toList

searchPerms :: Map Int [Int] -> IO PermStats
searchPerms g = do
    gen <- newStdGen
    let idp = mkIdPerm g
    let allPerms = iterate (genPerm g) (idp, gen)
    pure $ foldl' (addToStats g) emptyStats $ take 50000 allPerms
  where
